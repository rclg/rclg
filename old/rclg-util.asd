;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem :rclg-util
  :version "0.1.0"
  :depends-on (:rclg)
  :components
  ((:file "rclg-util")))
